package com.exigentech.mtgapiclient.cards.model;

public interface Card {
  String name();
}
